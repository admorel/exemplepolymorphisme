package implementation;

import model.armure.Armure;
import model.armure.armureLegere.ArmureCuir;
import model.armure.armureLourde.ArmureHarnois;
import model.armure.armureMedium.ArmureEcaille;

public class ExemplePolymorphisme {

    public static void main(String[] args) {

        Armure armureCuir = new ArmureCuir();
        Armure armureEcaille = new ArmureEcaille();
        Armure armureHarnois = new ArmureHarnois();

        System.out.println("armureCuir : " + armureCuir.getArmureCa());
        System.out.println("armureEcaille : " + armureEcaille.getArmureCa());
        System.out.println("armureHarnois : " + armureHarnois.getArmureCa());

    }
}
