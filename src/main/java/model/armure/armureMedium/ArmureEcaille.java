package model.armure.armureMedium;

import model.armure.ArmureMedium;

public class ArmureEcaille extends ArmureMedium {

    public ArmureEcaille() {
        super("Armure d'écaille", 4, "Ecaille de crocro");
    }
}
