package model.armure;

public class ArmureLourde extends Armure {

    public ArmureLourde(String armureNom, int armureCa, String armureDescription) {
        super(armureNom, armureCa, 5, 5, "Lourde", armureDescription);
    }
}
