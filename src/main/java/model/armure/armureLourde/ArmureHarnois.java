package model.armure.armureLourde;

import model.armure.ArmureLourde;

public class ArmureHarnois extends ArmureLourde {

    public ArmureHarnois() {
        super("Harnois", 1000, "si tu en porte un, tu vis");
    }
}
