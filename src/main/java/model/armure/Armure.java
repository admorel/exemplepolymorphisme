package model.armure;

/**
 * Classe mere des armures
 *
 * @author adrien
 */
public abstract class Armure {

    /**
     * armureNom : nom de l'armure (ex: Armure de cuir, armure de plaque...)
     */
    private String armureNom;

    /**
     * armureCa : valeur d'armure apporté par l'armure
     */
    private int armureCa;

    /**
     * malusDeplacement : nombre de case que le poids de l'armure engendre comme malus
     */
    private int malusDeplacement;

    /**
     * malusCaracterique : valeur du malus apporté aux caractéristique
     */
    private int malusCaracterique;

    /**
     * armureType : type de l'arme (légère, lourde, médium)
     */
    private String armureType;

    /**
     * armureDescription : description rp de l'armure
     */
    private String armureDescription;

    public Armure(String armureNom, int armureCa, int malusDeplacement, int malusCaracterique, String armureType, String armureDescription) {
        this.armureNom = armureNom;
        this.armureCa = armureCa;
        this.malusDeplacement = malusDeplacement;
        this.malusCaracterique = malusCaracterique;
        this.armureType = armureType;
        this.armureDescription = armureDescription;
    }

    public String getArmureNom() {
        return armureNom;
    }

    public void setArmureNom(String armureNom) {
        this.armureNom = armureNom;
    }

    public int getArmureCa() {
        return armureCa;
    }

    public void setArmureCa(int armureCa) {
        this.armureCa = armureCa;
    }

    public int getMalusDeplacement() {
        return malusDeplacement;
    }

    public void setMalusDeplacement(int malusDeplacement) {
        this.malusDeplacement = malusDeplacement;
    }

    public int getMalusCaracterique() {
        return malusCaracterique;
    }

    public void setMalusCaracterique(int malusCaracterique) {
        this.malusCaracterique = malusCaracterique;
    }

    public String getArmureType() {
        return armureType;
    }

    public void setArmureType(String armureType) {
        this.armureType = armureType;
    }

    public String getArmureDescription() {
        return armureDescription;
    }

    public void setArmureDescription(String armureDescription) {
        this.armureDescription = armureDescription;
    }
}
