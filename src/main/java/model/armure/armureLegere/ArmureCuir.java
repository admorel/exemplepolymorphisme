package model.armure.armureLegere;

import model.armure.ArmureLegere;

public class ArmureCuir extends ArmureLegere {

    public ArmureCuir() {
        super("Armure de cuir", 2, "Armure en cuir basique");
    }
}
