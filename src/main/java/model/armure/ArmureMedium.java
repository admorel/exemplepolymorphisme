package model.armure;

public class ArmureMedium extends Armure {

    public ArmureMedium(String armureNom, int armureCa, String armureDescription) {
        super(armureNom, armureCa, 2, 2, "Medium", armureDescription);
    }
}
