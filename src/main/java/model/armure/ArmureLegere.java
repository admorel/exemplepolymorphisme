package model.armure;

import model.armure.Armure;

public class ArmureLegere extends Armure {

    public ArmureLegere(String armureNom, int armureCa, String armureDescription) {
        super(armureNom, armureCa, 0, 0, "Légère", armureDescription);
    }
}
